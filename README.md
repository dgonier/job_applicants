# Instructions

## environment

* Create a python environment and install requirements.txt for django.
* Setup a Vue instance locally to make one component

## edit views.py

edit the process output function to return in the most optimized way possible
a list of dictionaries for all the values in the model.

Your api should be able to handle the following parameters when passed as a post request:

* isolate a particular index type
* isolate a particular organization_id
* aggregate values for a specifed date range start and end
* Please add additional parameters if desired

Note: to do this you will need to a weighted average where columns have the words 'AVG' or 'FREQ'

## consume the api

* Create a local Vue instance and build a data table component that can consume the output from your api
* The data table should be able to:
    * allow a user to filter based on search
    * allow a user to sort based on column
    
   
* Feel free to impress with style and features!

DONT WORRY ABOUT backgrounds, headers, menus etc. I am only curious about how the data table looks.


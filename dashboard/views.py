from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, status
from .models import *


# Create your views here.

class DashboardAPI(APIView):

    def init(self):
        self.parameters = {}
        self.data = None

    def process_output(self):

        output = Dashboard.objects

        ################ YOUR CODE GOES HERE ############



        #################################################

        return output.values()


    def post(self, request):
        self.data = request.data
        output = self.process_output()
        return Response(output, status=status.HTTP_200_OK)

from django.urls import path
from . import views

app_name = 'dashboard'

urlpatterns = [
    # Your URLs...
    path('', views.DashboardAPI.as_view(), name='dashboard'),
]

from django.db import models

# Create your models here.

class Dashboard(models.Model):
    sc_dt = models.DateTimeField(db_column='SC_DT', blank=True, null=True)
    time_type = models.CharField(db_column='TIME_TYPE', max_length=255, blank=True, null=True)
    organization_id = models.BigIntegerField(db_column='ORGANIZATION_ID', blank=True, null=True, db_index=True)
    employee_id = models.BigIntegerField(db_column='EMPLOYEE_ID', blank=True, null=True, db_index=True)
    parent_id = models.BigIntegerField(db_column='PARENT_ID', blank=True, null=True, db_index=True)
    id_name_helper = models.CharField(db_column='ID_NAME_HELPER', max_length=255, blank=True, null=True, db_index=True)
    index_type = models.CharField(db_column='INDEX_TYPE', max_length=255, blank=True, null=True, db_index=True)
    week_day = models.CharField(max_length=255, db_column='WEEK_DAY', blank=True, null=True, db_index=True)
    volume = models.FloatField(db_column='VOLUME', blank=True, null=True)
    ata_median = models.FloatField(db_column='ATA_MEDIAN', blank=True, null=True)
    ata_minus_pta_median = models.FloatField(db_column='ATA_MINUS_PTA_MEDIAN', blank=True, null=True)
    base_cost_avg = models.FloatField(db_column='BASE_COST_AVG', blank=True, null=True)
    base_cost_sum = models.FloatField(db_column='BASE_COST_SUM', blank=True, null=True)
    battery_volume = models.FloatField(db_column='BATTERY_VOLUME', blank=True, null=True)
    batt_test_on_batt_call_count = models.FloatField(db_column='BATT_TEST_ON_BATT_CALL_COUNT', blank=True, null=True)
    batt_test_on_batt_call_freq = models.FloatField(db_column='BATT_TEST_ON_BATT_CALL_FREQ', blank=True, null=True)
    call_accepted_count = models.FloatField(db_column='CALL_ACCEPTED_COUNT', blank=True, null=True)
    call_accepted_freq = models.FloatField(db_column='CALL_ACCEPTED_FREQ', blank=True, null=True)
    call_cost_avg = models.FloatField(db_column='CALL_COST_AVG', blank=True, null=True)
    call_cost_sum = models.FloatField(db_column='CALL_COST_SUM', blank=True, null=True)
    cancelled_count = models.FloatField(db_column='CANCELLED_COUNT', blank=True, null=True)
    cancelled_freq = models.FloatField(db_column='CANCELLED_FREQ', blank=True, null=True)
